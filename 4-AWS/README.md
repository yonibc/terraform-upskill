## 4-AWS

This TF template deploys:
 - a VPC with 2 private & 2 public subnets into 2 AZs.
 - a light Apache server with default configuration

### Deploy
1. `git clone https://chrisnagy@bitbucket.org/chrisnagy/terraform-upskill.git`
2. `cd terraform-upskill/4-AWS/`
3. `terraform init`
4. Make sure to set up working AWS CLI credentials
5. `terraform apply`

### Inputs
- `instance_type` - The instance type of the Apache server. Defaults to t3.small.
- `region` - Region to deploy to. Defaults to eu-central-1.

### Outputs
- `ec2_instance_public_ip`- The public IP of the Apache server instance.