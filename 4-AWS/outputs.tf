output "ec2_instance_public_ip" {
    value = aws_instance.apache_server.public_ip
    description = "The public IP address of the server 1 instance."
    # sensitive   = false
}