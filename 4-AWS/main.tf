######################################
### This TF template deploys:
# - a VPC with 2 private & 2 public subnets into 2 AZs.
# - a light apache server with default configuration
######################################

resource "aws_instance" "apache_server" {
    ami = "ami-08c148bb835696b45" # Amazon Linux 2 - eu-central-1 
    instance_type = var.instance_type

    subnet_id = module.vpc.public_subnets[0]
    security_groups = [aws_security_group.allow_http.id]
    associate_public_ip_address = true
    user_data = file("vm_startup.sh")

    tags = {
      Name = "tf-server"
      Terraform = "true"
      TF_Project = "4_AWS"
    }
}