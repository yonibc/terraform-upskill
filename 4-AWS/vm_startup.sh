#!/bin/bash

# update registry
yum update -y
# install Apache
yum install httpd -y
# start the Apache webserver
service httpd start