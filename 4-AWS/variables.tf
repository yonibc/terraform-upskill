variable "instance_type" {
    description = "The instance type of the Apache server. Defaults to t3.small."
    default = "t3.micro"
}

variable "region" {
    description = "Region to deploy to. Defaults to eu-central-1."
    default = "eu-central-1"
}
