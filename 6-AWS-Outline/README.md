## Info
This part of the upskill projects is an [Outline](https://getoutline.org/en/home) Shadowsocks server with an API to turn it on and off. This is an actively used project and since the server is deployed in my Nordclodu personal development AWS account, this is an efficient and very effective way to save on costs. Besides the manual off API endpoint, there is also a CloudWatch Action that spins down the instance after two hours of inactiviy.

The need for this server comes from public WIFI networks and their distaste for Google Meet. In hotels for example, Google Meet can not connect to one of the required websockets and therefore there is no audio or video data transfer between me and the meeting, effectively joining but not being able to communicate at all. One of the solutions is the new Nordcloud Aviatrix VPN, but that uses the OpenVPN protocol which is also very commonly blocked on such networks. This is why the need for a second VPN-like solution, and Outline has demonstrated its abilitis when it served as my only tunnel that worked consistently through the Great Firewall of China. That is not an easy feat due to their harsh censorship technologies, but I now trust Outline to pass through any network because its packets are almost identical to HTTPS.

## Architecture
The IaC tool used is Terraform. The single instance is of the size `t3.small` with an 8GB EBS root volume. Since shadowsocks is decently lightweight, there is no need for a bigger instance for a single user. An already existing S3 bucket is used for the EC2 instance to push the output of the Outline server installation process. The output contains the API key for the manager client app and is therefore necessary. For longevity purposes, the S3 based solution is better than reading the output from the EC2 console. After the script has finished installing everything, a `local exec` downloads the output file onto local.
The two Lambda functions are written in Python, and are deployed using purely Terraform's built-in artifact packaging solutions.

![infra diagram](static/infra-diagram.png)

## Deployment

## Usage
After Terraform pulled the API key from the S3 bucket, paste it into the Outline Manager application. This will connect to the server and client keys will be available. Paste the client key into the Outline client application. Outline Manager is available to download from Outline's website, and the client is available through the Mac App Store or from the website as well.

## Runbook

## To-Do
- Turn on & turn off Lambda functions
- API GW endpoints for `turn_on_hfhz383sj7` & `turn_off_h2jdj34735`, rate limited to one call per second.
- User Data script that installs the required software and is able to push the output to S3
- CW Action to automatically restart the instance if it reports unhealthy
- CW Action that monitors the instance and turns it off after 2 hours of inactivity