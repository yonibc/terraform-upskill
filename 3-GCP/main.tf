provider "google" {
  version = "3.5.0"
  credentials = file("key.json")
  project = var.project
  region  = var.region
  zone    = var.zone
}

resource "google_compute_network" "vpc_network" {
  name = "new-terraform-network"
  # auto_create_subnetworks = false
}

resource "google_compute_autoscaler" "foobar" {
    name = "gcp-autoscaler-foobar"
    project = var.project
    zone = var.zone
    target = google_compute_instance_group_manager.foobar.self_link

    autoscaling_policy {
        max_replicas = 5
        min_replicas = 2
        cooldown_period = 60

        cpu_utilization {
            target = 0.5
        }
    }
}

resource "google_compute_instance_template" "foobar" {
    name = "my-instance-template"
    machine_type = "n1-standard-1"
    can_ip_forward = false
    project = var.project
    tags = ["foo", "bar", "allow-lb-service"]
    metadata_startup_script = file("vm_startup.sh")

    disk {
        source_image = data.google_compute_image.centos_7.self_link
    }

    network_interface {
        network = "default"
    }

    service_account {
        scopes = ["userinfo-email", "storage-ro", "compute-ro"]
    }
}

module "lb" {
    source = "GoogleCloudPlatform/lb/google"
    version = "2.2.0"
    region = var.region
    name = "load-balancer"
    service_port = 80
    target_tags = ["my-target-pool", "allow-lb-service"]
    network = google_compute_network.vpc_network.name
}

resource "google_compute_target_pool" "foobar" {
    name = "my-target-pool"
    project = var.project
    region = var.region
}

resource "google_compute_instance_group_manager" "foobar" {
    name = "my-instance-group-manager"
    zone = var.zone
    project = var.project

    version {
        instance_template = google_compute_instance_template.foobar.self_link
        name = "primary"
    }

    target_pools = [google_compute_target_pool.foobar.self_link]
    base_instance_name = "terraform"
}

data "google_compute_image" "centos_7" {
  project = "centos-cloud"
  family = "centos-7"
  # name    = "centos-7-v20200902"
}