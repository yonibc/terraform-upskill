variable "project" {
    default = "playground-s-11-6b2b1ad8"
}

variable "region" {
    default = "us-central1"
}

variable "zone" {
    default = "us-central1-c"
}