### DISCLAIMER
# When you pick this up again, you'll have to get a new 'service-key.json' file used in 'provider.tf'
# To do this, open your new cloud playground and create a new "Service Account" in GCP
# Set it to be the owner of the whole project to avoid permission issues
# Replace the current 'service-key.json' file's contents with the new one

provider "google" {
    credentials = file("service-key.json")

    project = "playground-s-11-268c9f15"
    region = "us-central1"
    zone = "us-central1-c"
}

terraform {
    backend "gcs" {
        bucket = "chris-terraform-bucket"
        prefix = "terraform1"
        credentials = "service-key.json"
    }
}