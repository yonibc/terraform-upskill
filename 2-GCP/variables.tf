variable "project" {
    default = "playground-s-11-268c9f15"
}

variable "region" {
    default = "us-central1"
}

variable "zone" {
    default = "us-central1-c"
}

variable "cidr" {
    default = ["10.0.0.0/16"]
}