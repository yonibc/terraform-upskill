# README #

### What is this repository for? ###

* Storing all the files that are created during my learning path on Terraform.

### Contents
- 1-GCP
- 2-GCP
- 3-GCP
- [4-AWS](/4-AWS/) - VPC, SG, public Apache webserver

### How do I get set up? ###

* Make sure you are authenticated against Bitbucket by using an SSH key. [Guide](https://support.atlassian.com/bitbucket-cloud/docs/set-up-an-ssh-key/)

* (MacOS) Install Terraform
	* Install Homebrew `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"`
	* Install TF `brew install hashicorp/tap/terraform`
	* Make sure TF is working `terraform --help`

* Clone repository (HTTPS // SSH) `git clone https://chrisnagy@bitbucket.org/chrisnagy/terraform-upskill.git`


### Who do I talk to? ###

* Repo owner Chris Nagy - [chris.nagy@nordcloud.com](mailto:chris.nagy@nordcloud.com)