data "archive_file" "lambda_2" {
  source_dir  = "src/lambda_2/"
  output_path = "src/lambda_2.zip"
  type        = "zip"
}

resource "aws_lambda_function" "lambda_2" {
  function_name    = "${var.lambda_function_name}-2"
  handler          = var.lambda_handler
  role             = aws_iam_role.lambda_2_exec.arn
  runtime          = var.lambda_python_runtime
  timeout          = var.lamdba_timeout
  filename         = data.archive_file.lambda_2.output_path
  source_code_hash = data.archive_file.lambda_2.output_base64sha256
}

# This is redundant as it's already present in lambda_1.tf but keep it seperate for the sake of this project
resource "aws_iam_role" "lambda_2_exec" {
   name = "${var.lambda_function_name}-2-role"

   assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF

}