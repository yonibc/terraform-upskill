variable "region" {
    description = "The region for the deployment. Defaults to eu-central-1."
    default = "eu-central-1"
}

variable "lambda_function_name" {
    description = "The name of the Lambda function that will be created"
    default = "Terraform-Lambda"
}

variable "lambda_s3_bucket" {
    description = "The name of the bucket where the Lambda code's .zip file is stored"
    default = "nc-chris-tf-5-aws-lambda"
}

variable "lambda_python_runtime" {
    description = "The runtime version for Python Lambdas. Defaults to python3.8"
    default = "python3.8"
}

variable "lambda_handler" {
    description = "The handler for the Lambda function. Defaults to main.lambda_handler"
    default = "main.lambda_handler"
}

variable "lambda_1_version" {
    description = "The code version of the lambda_1 function. Used for finding the S3 key path. No default."
}

variable "lamdba_timeout" {
    description = "The default timeout for a Lambda function. Defaults to 3 seconds."
    default = 3
}