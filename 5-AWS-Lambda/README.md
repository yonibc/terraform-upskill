## Info

This is a practice project exploring how different deployment strategies for Lambda with Terraform are working, and their pros & cons. There are two primary ways outlined in this project, a bash script helper seen in `lambda_1`, one handled by Terraform `lambda_2` with no dependencies, and `lambda_3` also handled by Terraform's build modules with dependencies. This project only focuses on one Lambda function per deployment type.

### Update
After feedback from Timur, the second [and third] version seems to be the most commonly used one. Versioning Lambdas with those solutions can be done through git version control and redeployments. Unfortunately there is no great way right now to do Lambda versioning with pure Terraform in these scenarios.
Sometimes it can come up that the platform you package the lambda on is different than the AWS lambda runtime (eg. NumPy with C++). To achive a platform agnostic build environment, make sure to build using Docker containers (or the Amazon Linux 2 AMI).

#### Extra cases to explore:
- How to do canary / blue-green deployments with Terraform? Maybe Serverless/SAM is the better option for isolated Lambda stacks.

## Requirements
- Python3
- Terraform
- Valid AWS credentials

## Deployment
NOTICE: `lambda_1` is set up with a bash script to push/update the code, see `src/lambda_1/lambda_upload.sh`. It uses a manually set environment variable called `TF_VAR_lambda_1_version`. This environment variable is picked up by Terraform as well as by the build script. This is a fairly manual process and is error-prone, especially if the person is not familiar with the setup. Since this function is also part of the whole template, this has to be done in order to be able to deploy the "whole package".

0. This setup utilizes an `S3` backend. If you do not want to use that, simply delete the `backend.tf` file and your Terraform state files will be stored locally. If you do wish to use that, make sure to modify its content to correct values.
1. Run `terraform init` to initialize (download) all the necessary modules
2. Set env variable for Lambda_1 version - `export TF_VAR_lambda_1_version="1.0.1"`
3. Set env variable for Lambda_1 artifact bucket - `export TF_VAR_lambda_s3_bucket="nc-chris-tf-5-aws-lambda"`
4. Run `src/lambda_1/lambda_upload.sh` to package and upload the `lambda_1` function into its artifact bucket
5. Run `terraform apply` to deploy the architecture

## Upgrade
### Lambda_1
This upgrade process - both Terraform and the bash script - relies on the environment variables as seen below. To find out the current version of the lambda function, see the artifact bucket. If the version set in the environment variable is already used, the script will fail with an error code of 1. It will also let you know the latest version in the artifact bucket.
1. Modify `lambda_1`'s `main.py`
2. Set env variable for Lambda version - `export TF_VAR_lambda_1_version="1.0.2"`
3. Set env variable for Lambda artifact bucket - `export TF_VAR_lambda_s3_bucket="nc-chris-tf-5-aws-lambda"`
4. Run `src/lambda_1/lambda_upload.sh` to package and upload the `lambda_1` function into its artifact bucket
5. Run `terraform apply` to deploy the new architecture

### Lambda_2
There is no additional steps required to update the `lambda_2` function after its `main.py` changed. As seen in `lambda_2.tf`, Terraform will build and deploy the new version of the code automatically and checks for changes based on SHA hashes. The build process is done via Terraform's `archive_file` module.

### Lambda_3
This Lambda has dependencies and similarly to `lambda_2`, this mode of deployment/upgrade also doesn't need any manual inputs throughout the process. In addition it uses Terraform's built-in `local-exec` provisioner that executes the `src/lambda_3/build.sh` script, which in turn installs the required packages defined in `src/lambda_3/requirements.txt` into the `lambda_3` folder.

### Destroy
To destory the terraform infrastructure, simply run `terraform destory`.

## Cases to explore
- How to version Lambda functions through these kind of deployments? (see Update)
- How to roll back on the 2 & 3 type of deployment? (see Update)
- Is there an easier / more straighforward way to creating API Gateways? Modules in the Terraform Registry seem to be either too loosely documented or for very specific use-cases. (see Update - TF has no practical way currently AFAIK)

## Gotchas
- Terraform does not provide automatic API Gateway stage deployments. You have to taint the current stage of the API Gateway deployment for it to relase new changes with `terraform taint aws_api_gateway_deployment.<resource_name>`, then do `terraform apply` again for the changes to be deployed. It will not delete the current API Gateway, the URL will stay the same.
- `lambda_1`'s version should not exceed `9` at the second or third decimal place. This renders the `get_latest_lambda_1_version.sh` quite useless because AWS will list `1.1.1` after `1.0.10`.

## Resources
- [HashiCorp - Serverless Applications with AWS Lambda and API Gateway](https://learn.hashicorp.com/tutorials/terraform/lambda-api-gateway).
- [Building Lambda with terraform](https://aws-blog.de/2019/05/building-lambda-with-terraform.html)