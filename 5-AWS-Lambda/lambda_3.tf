resource "null_resource" "lambda_3_buildstep" {
  triggers = {
    handler      = "${base64sha256(file("src/lambda_3/main.py"))}"
    requirements = "${base64sha256(file("src/lambda_3/requirements.txt"))}"
    build        = "${base64sha256(file("src/lambda_3/build.sh"))}"
  }

  provisioner "local-exec" {
    command = "${path.module}/src/lambda_3/build.sh"
  }
}

data "archive_file" "lambda_3" {
  source_dir  = "${path.module}/src/lambda_3/"
  output_path = "${path.module}/src/lambda_3.zip"
  type        = "zip"

  depends_on = [null_resource.lambda_3_buildstep]
}

resource "aws_lambda_function" "lambda_3" {
  function_name    = "${var.lambda_function_name}-3"
  handler          = var.lambda_handler
  role             = aws_iam_role.lambda_3_exec.arn
  runtime          = var.lambda_python_runtime
  timeout          = var.lamdba_timeout
  filename         = data.archive_file.lambda_3.output_path
  source_code_hash = data.archive_file.lambda_3.output_base64sha256
}

# This is redundant as it's already present in lambda_1.tf but keep it seperate for the sake of this project
resource "aws_iam_role" "lambda_3_exec" {
   name = "${var.lambda_function_name}-3-role"

   assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF

}