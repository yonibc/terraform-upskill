#!/usr/bin/env bash

# Change to the script directory
cd "$(dirname "$0")"
mkdir -p package
pip3 install -r requirements.txt -t package/