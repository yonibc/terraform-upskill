#!/bin/bash
# List all objects at the lambda_1 path and tail the latest one (latest version)
## FIXME this will not work if the third decimal of the version - eg. 1.0.X - goes above 9.
## 1.0.11 is AWS alphabetically after 1.1.1
aws s3 ls s3://$TF_VAR_lambda_s3_bucket/artifacts/lambda_1/ | tail -1