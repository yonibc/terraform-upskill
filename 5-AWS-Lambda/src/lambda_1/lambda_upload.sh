#!/bin/bash

# Fail script if any of the commands fail
set -e

# Set up artifact S3 bucket name env variable
if [ -z "$TF_VAR_lambda_s3_bucket" ]
then
    echo "\nThe variable 'TF_VAR_lambda_s3_bucket' is empty!"
    read -p "🖐 Input the name of the bucket where the Lambda's code artifacts are / should be stored: " TF_VAR_lambda_s3_bucket
    echo "✅ TF_VAR_lambda_s3_bucket is SET at - $TF_VAR_lambda_s3_bucket"
else
    echo "✅ TF_VAR_lambda_s3_bucket is SET at - $TF_VAR_lambda_s3_bucket"
fi

# Set up artifact version env variable
if [ -z "$TF_VAR_lambda_1_version" ]
then
    echo "\nThe variable 'TF_VAR_lambda_1_version' is empty!"
    echo "❗️ The current latest version of this artifact is: " && aws s3 ls s3://$TF_VAR_lambda_s3_bucket/artifacts/lambda_1/ | tail -1
    # Check this 'awk' line tomorrow, it's from Moush's answer in Slack
    while [[ ! `echo $TF_VAR_lambda_1_version | awk -F '.' '$1 ~ /^[0-9]+$/ && $2 ~ /^[0-9]+$/ && $3 ~ /^[0-9]+$/ { print "YES" }'` = "YES" ]]; do
        echo "\n\nPlease set the new lambda_1_version env variable to one higher then the one shown above."
        echo "It must be in the format of '1.0.0', nothing else will be accepted!"
        read -p "🖐 TF_VAR_lambda_1_version: " TF_VAR_lambda_1_version
        echo "🖐 TF_VAR_lambda_1_version is SET at $TF_VAR_lambda_1_version"
        done
else
    echo "✅ TF_VAR_lambda_1_version is SET at $TF_VAR_lambda_1_version"
fi

# Check if just set version doesn't exist already
if aws s3 ls s3://$TF_VAR_lambda_s3_bucket/artifacts/lambda_1/ | grep $TF_VAR_lambda_1_version
then
    echo "🚫 Lambda code version ${TF_VAR_lambda_1_version} already exists! Exiting to avoid overwriting! Bump the version and try again."
    echo "🚫 Set the environment variable - TF_VAR_lambda_1_version - to one above $TF_VAR_lambda_1_version"
    exit 1
else
    echo "\n✅ New Lambda code version. Deploying."
    # Package main.py into lambda_1.zip
    cd src/lambda_1/
    zip lambda_1.zip main.py
    # Upload packaged Lambda .zip to S3 artifact bucket at the versioned key
    
    ## COMMENT FOR TESTING
    # echo "✅ Would have uploaded with version ${TF_VAR_lambda_1_version}"
    aws s3 cp lambda_1.zip s3://$TF_VAR_lambda_s3_bucket/artifacts/lambda_1/$TF_VAR_lambda_1_version/main.zip
    rm lambda_1.zip
fi