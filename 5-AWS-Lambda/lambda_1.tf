resource "aws_lambda_function" "lambda_1" {
   function_name = "${var.lambda_function_name}-1"
   # Where the Lambda's code is stored - eg. s3://my-bucket/artifacts/main.zip
   s3_bucket = var.lambda_s3_bucket
   s3_key    = "artifacts/lambda_1/${var.lambda_1_version}/main.zip"

   # "main" is the filename within the zip file (main.js) and "handler"
   # is the name of the property under which the handler function was
   # exported in that file.
   handler = var.lambda_handler
   runtime = var.lambda_python_runtime
   timeout = var.lamdba_timeout

   role = aws_iam_role.lambda_1_exec.arn
}

 # IAM role which dictates what other AWS services the Lambda function
 # may access.
resource "aws_iam_role" "lambda_1_exec" {
   name = "${var.lambda_function_name}-1-role"

   assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF

}