terraform {
  backend "s3" {
    bucket = "nc-chris-tf-backend"
    key    = "5-aws-lambda/terraform.tfstate"
    region = "eu-central-1"
  }
}