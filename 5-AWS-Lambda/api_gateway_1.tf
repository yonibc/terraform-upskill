# The API Gateway resource
resource "aws_api_gateway_rest_api" "apigw_1" {
  name        = "${var.lambda_function_name}-1"
  description = "The API Gateway connected to the ${var.lambda_function_name}-1 Lambda function."
}

# Apigw_1 to use proxy lambda integration
resource "aws_api_gateway_resource" "apigw_1_proxy" {
   rest_api_id = aws_api_gateway_rest_api.apigw_1.id
   parent_id   = aws_api_gateway_rest_api.apigw_1.root_resource_id
   path_part   = "{proxy+}"
}

# Apigw_1_proxy to use ANY method
resource "aws_api_gateway_method" "apigw_1_proxy" {
   rest_api_id   = aws_api_gateway_rest_api.apigw_1.id
   resource_id   = aws_api_gateway_resource.apigw_1_proxy.id
   http_method   = "ANY"
   authorization = "NONE"
}

# Integrate lambda_1 with apigw_1
resource "aws_api_gateway_integration" "lambda_1" {
   rest_api_id = aws_api_gateway_rest_api.apigw_1.id
   resource_id = aws_api_gateway_method.apigw_1_proxy.resource_id
   http_method = aws_api_gateway_method.apigw_1_proxy.http_method

   integration_http_method = "POST"
   type                    = "AWS_PROXY"
   uri                     = aws_lambda_function.lambda_1.invoke_arn
}

# Root proxy integration apigw
resource "aws_api_gateway_method" "apigw_1_proxy_root" {
   rest_api_id   = aws_api_gateway_rest_api.apigw_1.id
   resource_id   = aws_api_gateway_rest_api.apigw_1.root_resource_id
   http_method   = "ANY"
   authorization = "NONE"
}

# Root proxy integration between apigw_1 and lambda_1
resource "aws_api_gateway_integration" "lambda_1_root" {
   rest_api_id = aws_api_gateway_rest_api.apigw_1.id
   resource_id = aws_api_gateway_method.apigw_1_proxy_root.resource_id
   http_method = aws_api_gateway_method.apigw_1_proxy_root.http_method

   integration_http_method = "POST"
   type                    = "AWS_PROXY"
   uri                     = aws_lambda_function.lambda_1.invoke_arn
}

# Apigw deployment
resource "aws_api_gateway_deployment" "apigw_1" {
   depends_on = [
     aws_api_gateway_integration.lambda_1,
     aws_api_gateway_integration.lambda_1_root,
   ]

   rest_api_id = aws_api_gateway_rest_api.apigw_1.id
   stage_name  = "test"
}

# Permission for API GW to invoke the lambda_1 function
resource "aws_lambda_permission" "apigw_1" {
    statement_id = "AllowAPIGatewayInvoke"
    action = "lambda:InvokeFunction"
    function_name = aws_lambda_function.lambda_1.function_name
    principal = "apigateway.amazonaws.com"

    # The "/*/*" portion grants access from any method on any resource
    # within the API Gateway REST API.
    source_arn = "${aws_api_gateway_rest_api.apigw_1.execution_arn}/*/*"
}