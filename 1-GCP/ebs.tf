provider "aws" {}

resource "aws_ebs_volume" "hello_world" {
  availability_zone = "us-east-1a"
  size              = 40

  tags = {
    Name = "HelloWorld"
  }
}

# resource "aws_ebs_volume" "Lol" {
#     #imported existing resource
#     availability_zone = "us-east-1c"

#     tags = {
#         Name = "Lol"
#     }
# }